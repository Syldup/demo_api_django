from rest_framework import decorators, viewsets, status, permissions
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse
from api.serializers import *
from api.models import *
import json


class TableViewSet(viewsets.ModelViewSet):
    queryset = Table.objects.all()
    serializer_class = TableSerializer

    def create(self, request, *args, **kwargs):
        is_many = isinstance(request.data, list)
        if not is_many:
            return super(TableViewSet, self).create(request, *args, **kwargs)
        else:
            serializer = self.get_serializer(data=request.data, many=True)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        print(request.user.is_authenticated)
        if not request.user.is_authenticated:
            del request.data['num_back']
        return super(TableViewSet, self).update(request, *args, **kwargs)


@decorators.api_view(["GET", "POST"])
def compute_sum(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        if type(data) != list:
            return HttpResponse('', status=status.HTTP_400_BAD_REQUEST)
        data = [obj.values() for obj in data]
    else:
        data = list(Table.objects.values_list('num', 'num_back'))

    if not data:
        return JsonResponse({}, status=status.HTTP_200_OK)
    data = list(zip(*data))

    return JsonResponse({
        'num': sum(data[0]),
        'num_back': sum(data[1]),
    }, status=status.HTTP_200_OK)


@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def clear(request):
    print(request.user.is_authenticated)
    Table.objects.all().delete()
    return HttpResponse('', status=status.HTTP_200_OK)

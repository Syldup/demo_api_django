from rest_framework import routers
from rest_framework_simplejwt import views as jwt_views
from django.urls import path, include
from api.views import *
from rest_framework_swagger.views import get_swagger_view

router = routers.DefaultRouter()
router.register('table', TableViewSet)

urlpatterns = [
    path('auth/', include('rest_framework.urls')),
    path('sum/', compute_sum),
    path('clear/', clear),
    path('', include(router.urls)),
    path('token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('docs/', get_swagger_view(title="API Doc")),
]

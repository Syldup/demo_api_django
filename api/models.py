from django.db import models


class Table(models.Model):
    num = models.IntegerField(default=0)
    num_back = models.IntegerField(default=0)

## Install BDD PostgreSQL

Télécharger la dernière version de possgres (12.2)
[Postgresql-downloads](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads)

Lancer l'executable, Laisser les options par défaut.  
Vous pouvez choisire l'emplacement de posgre sur votre PC et le MDP admin
dans Option avencés coisire 'French, France' pour Locale.  
A la fin décocher ou fermer Stack Builder.

Vous pouver désormais accéder au base de donnée avec pgAdmin 4

### Créé un compte d'accète au donnée pour django
Dans le Brower, fair clique droit dans 'Login/Groupe Roles' et selectionner 'Create'  
Dans l'onglet 'General' de la popup, mettre 'django' comme Name  
Dans 'Définition' choisire un MDP, j'ai mis 'poipoi'me  
Et dans 'Privileges', selectionner 'Can login ?'  
Enfin Sauvevegarder

### Administrer la BDD
Les base de donner sont lister dans le Brower,  
J'ai utiliser la base par défaut (postgres) pour django,  
On pourra retrouver par la suite les table génerer pardjango
dans l'onglet 'Schemas' puis 'public'.


## Installer API Django (avec Pycharm)

Sur Pycharm, télécharger le projet en allant dans l'onglet 'VCS/Get from Version Control...'  
L'URL est : https://gitlab.com/Syldup/demo_api_django.git  
Choisir un répertoire et cliquer sur 'Clone'

### Créé un environement de développement python
Aller dans 'File/Settings/Project/Python Interpreter'  
Cliquer sur l'écroue pour 'Add...'  
Selectionner 'Virtualenv Evironment/New environment'  
Dans 'Base interpreter' selectionner Python37  
Choisir un répertoire et cliquer sur 'OK'  
Cliquer sur 'Apply' puis 'OK'

### Installer Djanger et le configurer
Ouvrez un terminal (avec Pycharm) verifier de vous êtes dans le venv,  
(le nom du venv doit s'afficher entre parenthèses devant le chemain d'accèt)  
Executer la commande : 
```
pip install -r requi.txt
```
Ouvrir 'api_django/settings.py'  
DATABASES comptien les information pour ce connecter à la BDD  
Modifier si nécessaire

#### Création des table dans la BDD
Executer dans le terminal :
```
python manage.py makemigrations api
python manage.py migrate
```

#### Créé un super utilisateur
Cette utilisateur sera utiliser pour se connecter à l'api
Executer dans le terminal :
```
python manage.py createsuperuser
```
Choisir un Username et password, l'email peut être bidon


### Demarer django
Executer dans le terminal :
```
python manage.py runserver
```

L'API est désormet accessible en local sur votre navigateur préférer
à l'adresse : http://127.0.0.1:8000/

Cette page n'existe pas mais ici, http://127.0.0.1:8000/api/  
On peut retrouver les URL générer avec django-rest-framework

J'ai mi en place une page swagger qui sert de documantation pour l'api
(il est uncomplet pour le moment)  
http://127.0.0.1:8000/api/docs/
